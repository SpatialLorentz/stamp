import math
import cStringIO
import itertools
import re
from copy import deepcopy
import datetime
import cPickle
import _heapq
##
#This script is the original version of the Simple Temporal Agent Mapping Planner (STAMP!)
#
##
perceptionDictionary = {}
PREDICATES = {}
GOALS = []

class Tree():
    def __init__(self):
        self.val = None
        self.branch = []
    def consumeString(self, string):
        #essentially here: val is either an and or an or otherwise its an item/fluent
        if(string[0] == '&'):#split into tree
            self.val = '&'
            
        return 0

def importProblem(str):
    problemFile = None
    problemFile = open(str, 'r')
    if(problemFile == None):
        return None
    Objects = {}
    ProblemName = None
    InitialState = {}
    line = 1
    Goal = []
    while(line != ""):
        line = problemFile.readline()
        line = line.strip()
        sLine = line.split()

        if(len(sLine) >= 1):
            if(sLine[0] == "Problem:"):
                line = problemFile.readline()
                line = line.strip()
                sLine = line.split(",")
                ProblemName = sLine[0]
            if(sLine[0] == "Objects:"):
                line = problemFile.readline()
                line = line.strip()
                if(line == 'None'):
                    continue
                sLine = line.split(",")
                for item in sLine:
                    item = item.strip()
                    Objects[item] = 1
            if(sLine[0] == "Goal:"):
                line = problemFile.readline()
                line = line.strip()
                sLine = line.split(",")
                #Change this to a list of tuples (goal, conditions), for example ((_X,_Y,_Z),_(X=contains=InSalad...))
                #perhaps build a tree and solve recursively.
                myGoalTree = Tree()
                myGoalTree.consumeString(line)
                for item in sLine:
                    item = item.strip()
                    Goal.append(item)

            if(sLine[0] == "Initial:"):
                line = problemFile.readline()
                line = line.strip()
                sLine = line.split(",")
                for item in sLine:
                    item = item.strip()
                    InitialState[item] = 1
    return (InitialState, Goal, Objects, ProblemName)

def importDomain(str):
    domainFile = None
    domainFile = open(str, 'r')
    if(domainFile == None):
        return None
    Predicates = {}
    DomainName = None
    line = 1
    while(line != ""):
        line = domainFile.readline()
        sLine = line.split()
        if(len(sLine) > 1):
            if(sLine[0] == "Domain:"):
                DomainName = sLine[1]
        if(len(sLine) == 1):
            if(sLine[0] == "Predicates"):
                line = domainFile.readline()
                while(True):
                    Pred = domainFile.readline()
                    Pred = Pred.strip()
                    if(Pred == "-:"):
                        break
                    Predicates[Pred] = (None, None)#preconditions,effects
                    line = domainFile.readline()
                    line = line.strip()
                    #beginning of preconditions:
                    if(line == ":-"):
                        #Preconditions
                        line = domainFile.readline()
                        line = line.strip()
                        line = line.split(":")
                        line[1] = line[1].strip()
                        Predicates[Pred] = (line[1],None)
                        #Effects
                        line = domainFile.readline()
                        line = line.strip()
                        line = line.split(":")
                        line[1] = line[1].strip()
                        Predicates[Pred] = (Predicates[Pred][0],line[1])
                        #Objects
                        line = domainFile.readline()
                        line = line.strip()
                        line = line.split(":")
                        line[1] = line[1].strip()
                        Predicates[Pred] = (Predicates[Pred][0],Predicates[Pred][1],line[1])
                        #Explicit Conditionals and Object conditions
                        line = domainFile.readline()
                        line = line.strip()
                        line = line.split(":")
                        line[1] = line[1].strip()
                        Predicates[Pred] = (Predicates[Pred][0],Predicates[Pred][1],Predicates[Pred][2], line[1])
                        #Cost of action
                        line = domainFile.readline()
                        line = line.strip()
                        line = line.split(":")
                        line[1] = int(line[1].strip())
                        Predicates[Pred] = (Predicates[Pred][0],Predicates[Pred][1],Predicates[Pred][2], Predicates[Pred][3], line[1])


                        line = domainFile.readline()
                        line = line.strip()
                        if(line == "-:"):
                            continue
                        print "ERROR, missing \"-: or :-\""

                        

            #lots of tricky stuff to happen here.

    return (DomainName, Predicates)

class WorldState():
    def __init__(self):
        self.stateNumber = 0
        self.objects = {}
        self.cost = 0
        self.fluents = {}
        self.actions = []
        self.groundActions = []
        self.heuristic = "ILC"
    def SetHeuristic(self, string):
        self.heuristic = string
        return 0
    def GetHeuristic(self):
        if(self.heuristic == "ILC"):
            i = len(GOALS)
            j = 0
            for item in self.fluents:
                if(item in GOALS):
                    j += 1

            return (i - j) ** 2
        return 0
    def SetFluents(self, dictionary = {}):
        for item in dictionary:
            self.fluents[item] = (dictionary[item])#simulates a deep copy of a dictionary
    def SetPredicates(self, dictionary = {}):
        for item in dictionary:
            PREDICATES[item] = (dictionary[item][0],dictionary[item][1],dictionary[item][2], dictionary[item][3], dictionary[item][4])#simulates a deep copy of a dictionary
    def SetObjects(self, dictionary = {}):
        for item in dictionary:
            self.objects[item] = (dictionary[item])
    def SetGoal(self, list):
        GOALS = list
    def GetVariables(self, string):
        list = []
        balance = 0
        previous = 0
        now = 0
        for item in string:
            if(item == '?'): previous = now
            if(item == ' ' or item == ')'): 
                    list.append(string[previous:now])
                    previous = now
            now += 1
        return list
    def GetNestedList(self, string):
        list = []
        balance = 0
        previous = 0
        now = 0
        for item in string:
            if(item == '('): balance = balance + 1
            if(item == ')'): balance = balance - 1
            if(item == ','):
                if(balance == 0):
                    list.append(string[previous:now])
                    previous = now
            now += 1
        list.append(string[previous:now])
        return list
    def GetCombinations(self, num):
        myList = []
        myList = itertools.permutations(self.objects.keys(), num)
        return myList
    def GetOrSplit(self, string):
        substring = ""
        balance = 0
        i = 0
        for item in string:
            if item == "(": balance += 1
            if item == ")": balance -= 1
            if item == ",":
                if balance == 0:
                    substring = string[0:i]
                    break
            i += 1
        rest = string[i+1:]
        return (substring,rest)
    def ConsumeAndValidate(self, string):
        if(string == ""):
            return True
        
        #print string
        #firstly remove operators and decide based on that
        #if(string.__contains__('_')):
        #    print "Cannot do Existential stuff yet, or something went wrong..."
        #    #get number of ?'s left
        #    
        #    
        #    return False
        if(string[1] == '&'):#we have an 'and'
            if(string[0] == "("):string = string[1:-1]
            string = string[2:]
            return self.ConsumeAndValidate(string)
            #so now, try again and see if we need to consume more operators
        if(string[1] == '|'):#we have an 'or'
            substr, rest = self.GetOrSplit(string)
            rest = rest.strip()
            if(substr[0] == "("):substr = substr[2:-1]
            #we need to now break it up into 'ands' 
            substr = substr.strip()
            stringCpy = substr.split(",")
            for item in stringCpy:
                item = item.strip()
                if(self.ConsumeAndValidate(item)):
                    return self.ConsumeAndValidate(rest)
            return False
        #otherwise, this is a fluent to check.
        stringCpy = string.split(",")
        first = stringCpy[0][1:-1]
        rest = ""
        if(len(stringCpy) > 1):
            rest = ",".join(stringCpy[1:])
        rest = rest.strip()
        #first should be a fluent, and contain no unground 
        if(not first.__contains__('_')):
            if(first in self.fluents):
                return self.ConsumeAndValidate(rest)
            if(first[0] == "!"):
                if(first[2:-1] not in self.fluents):
                    return self.ConsumeAndValidate(rest)
            return False
        else:     
            numOfExistential = string.count('_')
            stringsToCheck = []
            for combo in self.GetCombinations(numOfExistential):
                stringCPY = first
                for item in combo:
                    stringCPY = stringCPY.replace('_',item,1)
                stringsToCheck.append(stringCPY)
            if(first[0] == '!'):
                #none can exist so make sure they dont
                for existString in stringsToCheck:
                    if(existString[2:-1] in self.fluents):
                        return False
                if(rest == []):
                    return True
                return self.ConsumeAndValidate(rest)
            else:
                for existString in stringsToCheck:
                    if(existString in self.fluents):
                        if(rest == []):
                            return True
                        return self.ConsumeAndValidate(rest)
    def ExtractFluents(self, string):
        fluents = []
        items = re.findall(r'!\([A-Za-z0-9- ]+\)|\([A-Za-z0-9- ]+\)', string)
        for item in items:
            if item[0] == "(":
                fluents.append(item[1:-1])
                continue
            fluents.append(item)
        return fluents
    def ApplyFluents(self, list):
        #firstly for '!(Holding A)'
        # check if Holding A exists, if it does, replace it with this, else add !(Holding A)
        # for normally Holding A, if I am not holding A then I am now, delete it...
        for item in list:
            if(item[0] == '!'):
                check = item[2:-1]
                if(check in self.fluents):
                    del self.fluents[check]
                #self.fluents[item] = 1
            else:
                #check = '!('+item+')'
                #if(check in self.fluents):
                #    del self.fluents[check]
                self.fluents[item] = 1
    def GetNextStates(self):
        #NOTE TO SELF, this is where we would apply object LOGIC. such as Temporal Logic.
        successors = self.GetSuccessors()
        if(self.CheckIfComplete()):
            print "Solution found: ", self.actions
        newStates = []
        for item in successors:
            cpy = self.Copy()
            if(successors[item][1] != "None"):
                for newI in successors[item][1].split(","):
                    newI = newI.strip()
                    #we now look for a ~T= for a value assignment.
                    newVal = 1
                    if(newI.__contains__("~T=")):
                        #now we split again for the object and the value
                        newI = newI.split("~T=")
                        newVal = int(newI[1])
                        newI = newI[0]
                    if newI[0] == "-":
                        del cpy.objects[newI[1:]]
                    else:
                        cpy.objects[newI[1:]] = newVal
            cpy.actions.append(item)
            cpy.stateNumber += 1
            
            #cpy.groundActions.append(successors[item][2])
            #cpy.previousStateFluents1 = cPickle.loads(cPickle.dumps(cpy.previousStateFluents2, -1))
            #cpy.previousStateFluents2 = cPickle.loads(cPickle.dumps(self.fluents, -1))
            cpy.ApplyFluents(successors[item][0])
            #######extraval = 10
            ########Here we need to check if the goals have any of hte objects in them, if not then weight higher
            #######for obj in self.objects:
            #######    for goal in GOALS:
            #######        if(goal.__contains__(obj)):
            #######            extraval = 0
            #######            break
            #######    if(extraval == 0):
            #######        break
            cpy.cost = cpy.cost + successors[item][3] + 1
            #if(cpy.previousStateFluents1 == cpy.fluents):
            #    if(perceptionDictionary.has_key(cpy.groundActions[-2])):
            #        perceptionDictionary[cpy.groundActions[-2]].append(cpy.groundActions[-1])
            #    else:
            #        perceptionDictionary[cpy.groundActions[-2]] = [cpy.groundActions[-1]]
            #    continue
            newStates.append(cpy)
        return newStates
    def CheckIfComplete(self):
        i = 0
        for item in GOALS:
            if(item[0] == '!'):
                if(not self.fluents.has_key(item)):
                    i += 1
                    continue
            if(self.fluents.has_key(item)):
                i +=1
        if(i == len(GOALS)):
            self.Print("FO")
            return True
        return False
    def ApplyOperators(self, cnd, item):
        if(cnd[2] == "-="):
            self.objects[cnd[0]] = self.objects[cnd[0]] - int(cnd[3])
        if(cnd[2] == "+="):
            self.objects[cnd[0]] = self.objects[cnd[0]] + int(cnd[3])
        if(cnd[2] == "=="):
            if(self.objects[cnd[0]] == int(cnd[3])):
                return True
            else:
                return False
        if(cnd[2] == "<="):
            if(self.objects[cnd[0]] <= int(cnd[3])):
                return True
            else:
                return False
        if(cnd[2] == ">="):
            if(self.objects[cnd[0]] >= int(cnd[3])):
                return True
            else:
                return False
        return True;
    def GetSuccessors(self):
        #firstly we need to build a hash of each unbound variable
        successors = {}
        for item in PREDICATES:
            variables = {}
            #if(len(self.groundActions) > 0):
            #    if(perceptionDictionary.has_key(self.groundActions[-1])):
            #        if(item in perceptionDictionary[self.groundActions[-1]]):
            #            continue
            #get variables:
            if(item.__contains__('?')):
                for vars in self.GetVariables(item):
                    variables[vars] = None
            #now we need to copy the predicate and replace the variables with the replacements
            if(len(variables) > 0):
                for combo in self.GetCombinations(len(variables)):
                    i = 0
                    #NOW lets set a copy of the predicate to be replaced with new variables
                    cpyPred = PREDICATES[item][0]
                    cpyEffect = PREDICATES[item][1]
                    cpyObjects = PREDICATES[item][2]
                    cpyObjConds = PREDICATES[item][3]
                    for vars in variables:
                        variables[vars] = combo[i]
                        cpyPred = cpyPred.replace(vars, combo[i])
                        cpyEffect = cpyEffect.replace(vars, combo[i])
                        cpyObjects = cpyObjects.replace(vars, combo[i])
                        cpyObjConds = cpyObjConds.replace(vars, combo[i])
                        i += 1
                    cpyObjCondsL = cpyObjConds.split(",")
                    if(cpyObjConds != "None"):
                        cndMet = True
                        for cond in cpyObjCondsL:
                            con = cond.split()
                            con[0] = con[0].strip()
                            con[1] = con[1].strip()
                            #This is where arithmetic is done. BTW do this ONCE the contains or other thing has been completed... 
                            #for example dont apply to feta but apply to chopped feta when: =contains=Chopped is supplied.
                            arithcomp = []
                            for c in con:
                                arithcomp.append(c)
                            #Now dont actually apply this here, create a function that is called after the 'Contains' segment.
                           
                            #This is where WHERE and conditions are applied:
                            condition = con[1].split("=")
                            while(condition[0] == ""):
                                condition = condition[1:]
                            if(condition[0] == "!"):
                                if(condition[1] == "contains"):
                                    if(con[0].__contains__(condition[2])):
                                        cndMet = False
                                    else:
                                        cndMet = self.ApplyOperators(arithcomp,con[0])
                            if(condition[0] == "contains"):
                                if(condition[1] == "_"):
                                    cndMet = self.ApplyOperators(arithcomp,con[0])
                                if(condition[1] != "_"):
                                    if(not con[0].__contains__(condition[1])):
                                            cndMet = False
                                    else:
                                        cndMet = self.ApplyOperators(arithcomp,con[0])
                        if(not cndMet):
                            continue
                    #lets see if the predicate can be true
                    if(self.ConsumeAndValidate(cpyPred)):
                        #print "Successor: ", item, " Because conditions: ", cpyPred, " are met, applies: ",cpyEffect
                        itemN = item

                        for vars in variables:
                            itemN = itemN.replace(vars,variables[vars])
                        fluents = self.ExtractFluents(cpyEffect)
                        successors[itemN] = (fluents,cpyObjects, item, PREDICATES[item][4])
            else:
                cpyPred = PREDICATES[item][0]
                cpyEffect = PREDICATES[item][1]
                cpyObjects = PREDICATES[item][2]
                if(self.ConsumeAndValidate(cpyPred)):
                    #print "Successor: ", item, " Because conditions: ", cpyPred, " are met, applies: ",cpyEffect                  
                    fluents = self.ExtractFluents(cpyEffect)
                    successors[item] = (fluents,cpyObjects, item,PREDICATES[item][4])
        return successors
    def GetFluentString(self):
        flist = self.fluents.keys()
        for item in self.objects:
            flist.append(item)
            flist.append(str(self.objects[item]))
        flist.sort()
        return " ".join(flist)
    def Print(self, args="PFOG"):
        
        print "State: ", self.stateNumber
        print "Applied Actions: ", self.actions
        if("P"in args):
            print "Predicates:"
            for item in PREDICATES:
                print "\t", item
                print "\t"*2, "Preconditions: ", PREDICATES[item][0]
                print "\t"*2, "Effects: ", PREDICATES[item][1]
        if("F"in args):
            print "Fluents:"
            for item in self.fluents:
                print "\t", item
        if("O"in args):
            print "Objects:"
            for item in self.objects:
                print "\t", item, " #: ", self.objects[item]
        if("G"in args):
            print "Goal:"
            for item in GOALS:
                print item
    def Copy(self):
        return cPickle.loads(cPickle.dumps(self, -1))
        #return deepcopy(self)


folder = raw_input("Please supply name of location folder: ")
heuristic = raw_input("Please supply heuristic name (default is squared inverse landmark counting): ILC^2: ")
if(heuristic == ""): heuristic = "ILC"
timestamp1 = datetime.datetime.now()
name, PREDICATES = importDomain("ExDomainProb\{}\Domain.txt".format(folder))
InitialState, GOALS, Objects, ProblemName = importProblem("ExDomainProb\{}\Problem.txt".format(folder))
InitialWorldState = WorldState()
InitialWorldState.SetFluents(InitialState)
InitialWorldState.SetObjects(Objects)
InitialWorldState.SetHeuristic(heuristic)
InitialWorldState.Print()
#InitialWorldState.GetSuccessors()
newStates = []#[InitialWorldState]so
_heapq.heappush(newStates, (0, InitialWorldState))
stateDictionary = {}
overlap = 0
total = 1
#we need to remove the ability to do certain actions after doing a specific action
#to do this we need to generate a bunch of starting positions such that when we specify an N we can generate successor states and propagate.
#so that if at any point returns to a previously seen point then we disallow the running of that sequence to N
#eg lets say we start with PickUpKnife() then GoToFridge() then GoToTable() -- at this point we have already seen all the fluents and done back
#tracking using the predicates, so we take the first action PickUpKnife() and disallow GoToFridge()
#same for PickUpKnife() then DropKnife()
#however since we are generating these sequences for any State we have to assume that any fluent is available EXCEPT for those that are explicitly there
#already, so if a predicate requires that we dont have a knife in our hands and we do then we cant do that predicate,
#however if we instead have the same problem but we dont have a fluent describing the knife in our hands then we assume it succeeds.
while (len(newStates) != 0):
    solution = False 
    item = _heapq.heappop(newStates)[1]
    for item2 in item.GetNextStates():
        flstring = item2.GetFluentString()
        if(not stateDictionary.has_key(flstring)):
            stateDictionary[flstring] = item2.stateNumber
            _heapq.heappush(newStates, (item2.cost + item2.GetHeuristic(), item2))
            #newNewStates.append(item2)
            if(item2.CheckIfComplete()):
                solution = True
                break
        else:
            overlap += 1
        total += 1
    if(solution):
        break
    #newStates = cPickle.loads(cPickle.dumps(newNewStates, -1))
    if(len(newStates) == 0):
        print "Could Not find a solution"
timestamp2 = datetime.datetime.now()
#for item in stateDictionary:
#    print item, " : ", stateDictionary[item]
#if solution:
print "Solution took: ", timestamp2 - timestamp1, " to compute; with: ", overlap, " overlap / unnecessary computation calls (drop knife issue): ", float(overlap)/total,"%"

#you could use string matching from the goals and the objects to see what ones to weight highest...