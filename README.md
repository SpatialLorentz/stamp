# README #

### What is this repository for? ###

* A Simple offline Planning python module.
(Which will eventually incorporate temporal planning)
* version: 0.1_2

### How do I get set up? ###

* Download the python file and the example folders containing the supplied domains and problems of BlocksWorld and Salad
* Requires:
python modules: math, itertools, re, copy, datetime
* To run, execute the python module from the terminal and it will prompt you for a folder containing a Problem and Domain file, supply the folder name and it will parse the contents and attempt to solve.
There is currently no verbose mode for failed execution (it is a task which we will get around to completing).
for example:
![Example.PNG](https://bitbucket.org/repo/nn8peA/images/2029084445-Example.PNG)
This when run will hopefully output something similar to: 
![Example2.PNG](https://bitbucket.org/repo/nn8peA/images/2056439212-Example2.PNG)

### Contribution guidelines ###

* Currently contribution is not allowed, this will be opened up in the future.
This Planner is only for the use of academic, and not-for-profit organizations and researchers.

### Who do I talk to? ###

* Me, at Perrenspence@gmail.com